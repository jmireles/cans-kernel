module gitlab.com/jmireles/cans-kernel

go 1.17

require (
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad
	gotest.tools/v3 v3.1.0
)

require (
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
