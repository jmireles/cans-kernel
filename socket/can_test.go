package socket

import (
	"fmt"
	"testing"
	"unsafe"

	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

// If this mocks ever starts failing, the documentation needs to be updated
// to prefer pass-by-pointer over pass-by-value.
func TestCAN_Size(t *testing.T) {
	assert.Assert(t, unsafe.Sizeof(CAN{}) <= 16, "CAN size is <= 16 bytes")
}

func TestCAN_Validate(t *testing.T) {
	for _, tt := range []CAN{
		{ID: canMaxId + 1},
		{ID: canMaxExtId + 1, IsExtended: true},
	} {
		tt := tt
		t.Run(fmt.Sprintf("%v", tt), func(t *testing.T) {
			assert.Check(t, tt.Validate() != nil, "should return validation error")
		})
	}
}

func TestCAN_String(t *testing.T) {
	for _, tt := range []struct {
		can CAN
		str string
	}{
		{
			can: CAN{ID: 0x62e, Length: 2, Data:Data{0x10, 0x44}},
			str: "62E#1044",
		},
		{
			can: CAN{ID: 0x410, IsRemote: true, Length: 3},
			str: "410#R3",
		},
		{
			can: CAN{ID: 0xd2, Length: 2, Data: Data{0xf0, 0x31}},
			str: "0D2#F031",
		},
		{
			can: CAN{ID: 0xee},
			str:   "0EE#",
		},
		{
			can: CAN{ID: 0},
			str:   "000#",
		},
		{
			can: CAN{ID: 0, IsExtended: true},
			str:   "00000000#",
		},
		{
			can: CAN{ID: 0x1234abcd, IsExtended: true},
			str:   "1234ABCD#",
		},
	} {
		tt := tt
		t.Run(fmt.Sprintf("String|can=%v,str=%v", tt.can, tt.str), func(t *testing.T) {
			assert.Check(t, cmp.Equal(tt.str, tt.can.String()))
		})
		t.Run(fmt.Sprintf("UnmarshalString|can=%v,str=%v", tt.can, tt.str), func(t *testing.T) {
			var actual CAN
			if err := actual.UnmarshalString(tt.str); err != nil {
				t.Fatal(err)
			}
			assert.Check(t, cmp.DeepEqual(actual, tt.can))
		})
	}
}

func TestCAN_Errors(t *testing.T) {
	for _, tt := range []string{
		"foo",                    // invalid
		"foo#",                   // invalid ID
		"0D23#F031",              // invalid ID length
		"62E#104400000000000000", // invalid data length
	} {
		tt := tt
		t.Run(fmt.Sprintf("str=%v", tt), func(t *testing.T) {
			var can CAN
			err := can.UnmarshalString(tt)
			assert.ErrorContains(t, err, "invalid")
			assert.Check(t, cmp.DeepEqual(CAN{}, can))
		})
	}
}
