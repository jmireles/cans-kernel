package errs

import (
	"encoding/hex"
	"fmt"
)

type Controller uint8

const (
	ControllerUnspecified      Controller = 0x00
	ControllerRxBufferOverflow Controller = 0x01
	ControllerTxBufferOverflow Controller = 0x02
	ControllerRxWarning        Controller = 0x04
	ControllerTxWarning        Controller = 0x08
	ControllerRxPassive        Controller = 0x10
	ControllerTxPassive        Controller = 0x20 // at least one error counter exceeds 127
	ControllerActive           Controller = 0x40
)

type Class uint32

const (
	ClassTxTimeout         Class = 0x00000001
	ClassLostArbitration   Class = 0x00000002
	ClassController        Class = 0x00000004
	ClassProtocolViolation Class = 0x00000008
	ClassTransceiver       Class = 0x00000010
	ClassNoAck             Class = 0x00000020
	ClassBusOff            Class = 0x00000040
	ClassBus          Class = 0x00000080
	ClassRestarted         Class = 0x00000100
)

type Frame struct {
	// Class is the error class
	Class Class
	// LostArbitrationBit contains the bit number when the error class is LostArbitration.
	LostArbitrationBit uint8
	// Controller contains error information when the error class is Controller.
	Controller Controller
	// ProtocolViolation contains error information when the error class is Protocol.
	Protocol ProtocolViolation
	// ProtocolViolationLoc contains error location when the error class is Protocol.
	ProtocolViolationLoc ProtocolViolationLoc
	// Transceiver contains error information when the error class is Transceiver.
	Transceiver Transceiver
	// ControllerSpecificInfo contains controller-specific additional error information.
	ControllerSpecificInfo [3]byte
}

func (e *Frame) String() string {
	switch e.Class {
	case ClassLostArbitration:
		return fmt.Sprintf("%v in bit %d (%s)",
			e.Class,
			e.LostArbitrationBit,
			hex.EncodeToString(e.ControllerSpecificInfo[:]),
		)
	case ClassController:
		return fmt.Sprintf("%v: %v (%s)",
			e.Class,
			e.Controller,
			hex.EncodeToString(e.ControllerSpecificInfo[:]),
		)
	case ClassProtocolViolation:
		return fmt.Sprintf("%v: %v: location %v (%s)",
			e.Class,
			e.Protocol,
			e.ProtocolViolationLoc,
			hex.EncodeToString(e.ControllerSpecificInfo[:]),
		)
	case ClassTransceiver:
		return fmt.Sprintf("%v: %v (%s)",
			e.Class,
			e.Transceiver,
			hex.EncodeToString(e.ControllerSpecificInfo[:]),
		)
	default:
		return fmt.Sprintf("%v (%s)",
			e.Class,
			hex.EncodeToString(e.ControllerSpecificInfo[:]),
		)
	}
}

type ProtocolViolation uint8

const (
	ProtocolViolationUnspecified ProtocolViolation = 0x00
	ProtocolViolationSingleBit   ProtocolViolation = 0x01
	ProtocolViolationFrameFormat ProtocolViolation = 0x02
	ProtocolViolationBitStuffing ProtocolViolation = 0x04
	ProtocolViolationBit0        ProtocolViolation = 0x08 // unable to send dominant bit
	ProtocolViolationBit1        ProtocolViolation = 0x10 // unable to send recessive bit
	ProtocolViolationBusOverload ProtocolViolation = 0x20
	ProtocolViolationActive      ProtocolViolation = 0x40 // active error announcement
	ProtocolViolationTx          ProtocolViolation = 0x80 // error occurred on transmission
)

type ProtocolViolationLoc uint8

const (
	ProtocolViolationLocUnspecified    ProtocolViolationLoc = 0x00
	ProtocolViolationLocStartOfFrame   ProtocolViolationLoc = 0x03
	ProtocolViolationLocID28To21       ProtocolViolationLoc = 0x02 // standard frames: 10 - 3
	ProtocolViolationLocID20To18       ProtocolViolationLoc = 0x06 // standard frames: 2 - 0
	ProtocolViolationLocSubstituteRTR  ProtocolViolationLoc = 0x04 // standard frames: RTR
	ProtocolViolationLocIDExtension    ProtocolViolationLoc = 0x05
	ProtocolViolationLocIDBits17To13   ProtocolViolationLoc = 0x07
	ProtocolViolationLocIDBits12To05   ProtocolViolationLoc = 0x0F
	ProtocolViolationLocIDBits04To00   ProtocolViolationLoc = 0x0E
	ProtocolViolationLocRTR            ProtocolViolationLoc = 0x0C
	ProtocolViolationLocReservedBit1   ProtocolViolationLoc = 0x0D
	ProtocolViolationLocReservedBit0   ProtocolViolationLoc = 0x09
	ProtocolViolationLocDataLengthCode ProtocolViolationLoc = 0x0B
	ProtocolViolationLocData           ProtocolViolationLoc = 0x0A
	ProtocolViolationLocCRCSequence    ProtocolViolationLoc = 0x08
	ProtocolViolationLocCRCDelimiter   ProtocolViolationLoc = 0x18
	ProtocolViolationLocACKSlot        ProtocolViolationLoc = 0x19
	ProtocolViolationLocACKDelimiter   ProtocolViolationLoc = 0x1B
	ProtocolViolationLocEndOfFrame     ProtocolViolationLoc = 0x1A
	ProtocolViolationLocIntermission   ProtocolViolationLoc = 0x12
)

type Transceiver uint8

const (
	TransceiverUnspecified     Transceiver = 0x00
	TransceiverCANHNoWire      Transceiver = 0x04
	TransceiverCANHShortToBat  Transceiver = 0x05
	TransceiverCANHShortToVCC  Transceiver = 0x06
	TransceiverCANHShortToGND  Transceiver = 0x07
	TransceiverCANLNoWire      Transceiver = 0x40
	TransceiverCANLShortToBat  Transceiver = 0x50
	TransceiverCANLShortToVcc  Transceiver = 0x60
	TransceiverCANLShortToGND  Transceiver = 0x70
	TransceiverCANLShortToCANH Transceiver = 0x80
)

// error frame flag indices.
const (
	// IndexLostArbitrationBit is the byte index of the lost arbitration bit in an error frame.
	IndexLostArbitrationBit = 0
	// IndexController is the byte index of the controller error in an error frame.
	IndexController = 1
	// IndexProtocol is the byte index of the protocol error in an error frame.
	IndexProtocol = 2
	// IndexProtocolViolationLoc is the byte index of the protocol error location in an error frame.
	IndexProtocolViolationLoc = 3
	// IndexTransceiver is the byte index of the transceiver error in an error frame.
	IndexTransceiver = 4
	// IndexControllerSpecificInfo is the starting byte of controller specific information in an error frame.
	IndexControllerSpecificInfo = 5
	// LengthControllerSpecificInfo is the number of error frame bytes with controller-specific information.
	LengthControllerSpecificInfo = 3
)

