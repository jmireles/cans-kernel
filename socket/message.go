package socket

// Message is anything that can marshal and unmarshal itself to/from a CAN frame.
type Message interface {
	CANMarshaler
	CANUnmarshaler
}

// FrameMarshaler can marshal itself to a CAN frame.
type CANMarshaler interface {
	MarshalCAN() (CAN, error)
}

// FrameUnmarshaler can unmarshal itself from a CAN frame.
type CANUnmarshaler interface {
	UnmarshalCAN(CAN) error
}
